import UsersList from './components/UsersList/UsersList';
import UserItem from './components/UserItem/UserItem';

function App() {
  return (
    <div className="App">
      <UsersList />
    </div>
  );
}

export default App;
