import React, {useEffect, useState} from 'react' ;
import axios from 'axios';
import UserItem from '../UserItem/UserItem';
import UserDetail from '../UserDelail/UserDetail';

const useFetch =() => {
    const [data, updateDate] = useState(null);
    const requestUrl = "https://reqres.in/api/users?page=2";
    useEffect(()=> {
        const fetchData = async() => {
            const response = await axios.get(requestUrl);
            updateDate(response.data);
        }
        fetchData();
    }, []);
    return data;
}

const UsersList = ()=> {
    const result = useFetch();
    const [user, changeUser] = useState(null);
    const showFullInfo = id => {
        const  currentUser = result.data.filter(item => {
            return item.id == id
        })
        changeUser(currentUser[0]);
    }
    return (
        <div className ="usersList">
            <ul>
                {
                    result && result.data.map((item, index) => {
                        return (
                            <UserItem
                                key = {index}
                                firstName = {item.first_name}
                                lastName = {item.last_name}
                                prepareData = {()=> showFullInfo(item.id) }
                            />
                        )
                    })
                }
            </ul>
            {result && user ? <UserDetail userInfo = {user}/> : null}
        </div>
    )

    
}

export default UsersList;